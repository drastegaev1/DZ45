import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chromium.ChromiumDriver;

public class BaseTest {
    WebDriver driver;

    @BeforeEach
    public void setUp(){
        WebDriverManager.chromiumdriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }
    @AfterEach
    public void close(){
        if(driver != null)
            driver.quit();
    }
//    @Attachment(value = "screen", type = "image/png")
//    public byte[] getScreenshot(){
//        return  ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
//    }
}
