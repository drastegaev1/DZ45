package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PochtaMainPage {
    private static final Logger logger = LogManager.getLogger(PochtaMainPage.class); // переменная класса для логирования
    By searchLogIn = new By.ByCssSelector("a[href='/api/auth/login']");
    By username = new By.ByCssSelector("#username");
    By userspassword = new By.ByCssSelector("#userpassword");
    By myshipments = new By.ByXPath("//div[@title = '79917133197']");
    By tracking = new By.ByCssSelector("#tippy-1 a[href='/tracking']");
    By text = new By.ByXPath("//*[@id='page-tracking']/div/div[2]/div[2]/div[1]/div/div[1]/div/p");

    public PochtaMainPage(WebDriver driver) {
        this.driver = driver;
    }

    WebDriver driver;

    @Step("Открыть страницу https://www.pochta.ru/")
    public PochtaMainPage open() {
        driver.get("https://www.pochta.ru/");
        logger.info("Открытие страницы https://www.pochta.ru/");
        return this;
    }

    @Step("Перейти на страницу авторизации")
    public void LogIn() {
        driver.findElement(searchLogIn).sendKeys(Keys.ENTER);
        logger.info("Кнопка 'Войти' найдена и нажата");
    }

    @Step("Ввести имя пользователя {user}")
    public void enterUserName(String user) {
        driver.findElement(username).sendKeys(user + Keys.ENTER);
        logger.info("Введено имя пользователя {user}");
    }

    @Step("Ввести пароль")
    public void enterUserPassword(String password) {
        driver.findElement(userspassword).sendKeys(password + Keys.ENTER);
        logger.info("Введен пароль {password}");
    }

    @Step("Перейти на вкладку 'Мои отправления'")
    public void tabMyShipments() {
        Actions actions = new Actions(driver);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(myshipments)));
        actions.moveToElement(driver.findElement(myshipments)).build().perform();
        //второй вариант работы с ожиданием
//        actions.moveToElement(wait.until(ExpectedConditions.visibilityOf(driver.findElement(myshipments)))).build().perform();

        logger.info("Меню пользователя открыто'");
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(tracking)));
        driver.findElement(tracking).click();
        logger.info("Выбран пункт меню'Мои отправления'");
    }

    @Step("Проверить текст")
    public String checkText() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        String s = wait.until(ExpectedConditions.visibilityOf(driver.findElement(text))).getText();
        logger.info("Сохранение значения элемента");
//        String s = "Сохраняйте отправления, чтобы узнавать по электронной почте, где они находятся";
        return s;
    }
}
