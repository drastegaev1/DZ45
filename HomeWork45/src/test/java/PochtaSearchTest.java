import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.PochtaMainPage;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

@DisplayName("Тесты страницы pochta.ru")
public class PochtaSearchTest extends BaseTest{
    private static final Logger logger = LogManager.getLogger(PochtaMainPage.class);
    String expected = "Сохраняйте отправления, чтобы узнавать по электронной почте, где они находятся";
    @Test
    @DisplayName("Работа со страницей https://www.pochta.ru/")
    public void workPochta(){
        PochtaMainPage pochtaMainPage = new PochtaMainPage(driver);
        pochtaMainPage.open();
        logger.info("Страница https://www.pochta.ru/ открыта");
        pochtaMainPage.LogIn();
        logger.info("Страница авторизации открыта");
        pochtaMainPage.enterUserName("tojoxi2718@mahazai.com");
        pochtaMainPage.enterUserPassword("1QAZ2wsx");
        logger.info("Авторизация пройдена");
        pochtaMainPage.tabMyShipments();
        logger.info("Вкладка 'Мои отправления' открыта");
        Assertions.assertEquals(expected, pochtaMainPage.checkText());
        logger.info("Проверка текста пройдена!");
    }


}
